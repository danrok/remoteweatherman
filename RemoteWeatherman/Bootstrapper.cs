﻿using Autofac;
using RemoteWeatherman.Core.Implementation;
using RemoteWeatherman.Core.Implementation.Configuration;
using RemoteWeatherman.Core.Interfaces.Configuration;
using Windows.UI.Xaml;

namespace RemoteWeatherman
{
    public class Bootstrapper
    {
        ContainerBuilder builder;

        public Bootstrapper()
        {
            builder = new ContainerBuilder();
        }

        public IContainer InitializeContainer(string deviceName, int intervalMs)
        {
            builder.RegisterType<AzureConfigurationManager>().AsImplementedInterfaces();
            builder.RegisterType<ConfigurationManager>().AsImplementedInterfaces();

            builder.RegisterType<AzureHubService>().AsImplementedInterfaces();

            builder.RegisterType<Bmp180Sensor>().AsImplementedInterfaces().WithParameter("deviceName", deviceName);
            builder.RegisterType<DispatcherTimer>().As<DispatcherTimer>();

            builder.RegisterType<RemoteSensorService>().AsImplementedInterfaces();
            builder.RegisterType<TimeManagerService>().AsImplementedInterfaces();

            builder.RegisterType<WeathermanService>().AsImplementedInterfaces().WithParameter("intervalMs", intervalMs);

            var container = builder.Build();
            return container;
        }
    }
}
