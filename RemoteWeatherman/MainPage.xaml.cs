﻿using System;
using Autofac;
using Windows.UI.Xaml.Controls;
using RemoteWeatherman.Core.Interfaces;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace RemoteWeatherman
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        IContainer container;

        public MainPage()
        {
            this.InitializeComponent();
            Bootstrapper bootstrapper = new Bootstrapper();
            container = bootstrapper.InitializeContainer("sampleDevice", 60000);
            StartRemoteWeatherman();
        }

        private void StartRemoteWeatherman()
        {
            var weathermanService = container.Resolve<IWeathermanService>();
            weathermanService.Start();
        }
    }
}
