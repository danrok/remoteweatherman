﻿using System;
using RemoteWeatherman.Core.Interfaces;
using Windows.UI.Xaml;

namespace RemoteWeatherman.Core.Implementation
{
    public class TimeManagerService : ITimeManagerService
    {
        private DispatcherTimer timer;

        public TimeManagerService(DispatcherTimer timer)
        {
            this.timer = timer;
        }

        public void ConfigureTimeManagerService(int readingIntervalMs, Action<object, object> intervalMethod) 
        {
            this.timer.Interval = TimeSpan.FromMilliseconds(readingIntervalMs);
            this.timer.Tick += new EventHandler<object>(intervalMethod);
        }

        public void StartTimer()
        {
            timer.Start();
        }

        public void StopTimer()
        {
            timer.Stop();
        }
    }
}
