﻿using System;
using RemoteWeatherman.Core.Interfaces;
using RemoteWeatherman.Core.Interfaces.Enums;
using System.Threading.Tasks;
using RemoteWeatherman.Core.Interfaces.Entities;

namespace RemoteWeatherman.Core.Implementation
{
    public class RemoteSensorService : IRemoteSensorService
    {
        IBmp180Sensor sensor;

        public RemoteSensorService(IBmp180Sensor sensor)
        {
            this.sensor = sensor;
        }

        public async void InitializeSensor()
        {
            await sensor.InitializeAsync();
        }

        public async Task<SensorData> GetSensorReading()
        {
            SensorData sensorData = null;

            try
            {
                sensorData = await sensor.GetSensorDataAsync(Bmp180AccuracyMode.UltraHighResolution);
            }
            catch
            {
                //Log exception
            }

            return sensorData;
        }
    }
}
