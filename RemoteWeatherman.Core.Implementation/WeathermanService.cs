﻿using System;
using RemoteWeatherman.Core.Interfaces;

namespace RemoteWeatherman.Core.Implementation
{
    public class WeathermanService : IWeathermanService
    {
        private IAzureHubService azureService;
        private ITimeManagerService timeManagerService;
        private IRemoteSensorService remoteSensorService;
        private Action<object, object> intervalMethod;
        private int intervalMs;

        public WeathermanService(IRemoteSensorService remoteSensorService, IAzureHubService azureService, ITimeManagerService timeManagerService, int intervalMs)
        {
            this.azureService = azureService;
            this.timeManagerService = timeManagerService;
            this.remoteSensorService = remoteSensorService;
            this.intervalMs = intervalMs;
            intervalMethod = (object sender, object e) => ReadSensorDataAndPushToAzure();
        }

        public void Start()
        {
            remoteSensorService.InitializeSensor();
            azureService.InitializeAzureHubClient();
            timeManagerService.ConfigureTimeManagerService(intervalMs, intervalMethod);
            timeManagerService.StartTimer();
        }

        private async void ReadSensorDataAndPushToAzure()
        {
            try
            {
                var sensorData = await remoteSensorService.GetSensorReading();
                azureService.SendDeviceToCloudMessagesAsync(sensorData);
            }
            catch
            {
                // Log exception here
            }
        }

    }
}
