﻿using System;
using RemoteWeatherman.Core.Interfaces.Configuration;
using RemoteWeatherman.Core.Interfaces.Entities;
using System.Threading.Tasks;

namespace RemoteWeatherman.Core.Implementation.Configuration
{
    public class AzureConfigurationManager : IAzureConfigurationManager
    {
        IConfigurationManager configurationManager;

        public AzureConfigurationManager(IConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public async Task<AzureConfiguration> ReadAzureConfiguration()
        {
            var azureConfiguration = new AzureConfiguration();
            var xmlConfig = await configurationManager.GetConfig();

            azureConfiguration.DeviceId = configurationManager.GetAppSettingsValue(xmlConfig, "deviceId");
            azureConfiguration.IotHubUri = configurationManager.GetAppSettingsValue(xmlConfig, "iotHubUri");
            azureConfiguration.DeviceKey = configurationManager.GetAppSettingsValue(xmlConfig, "deviceKey");

            return azureConfiguration;
        }
    }
}
