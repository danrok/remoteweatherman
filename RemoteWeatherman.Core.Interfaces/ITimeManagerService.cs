﻿using System;

namespace RemoteWeatherman.Core.Interfaces
{
    public interface ITimeManagerService
    {
        void ConfigureTimeManagerService(int readingIntervalMs, Action<object, object> intervalMethod);
        void StartTimer();
        void StopTimer();
    }
}
