﻿using Windows.Data.Xml.Dom;
using System.Threading.Tasks;

namespace RemoteWeatherman.Core.Interfaces.Configuration
{
    public interface IConfigurationManager
    {
        Task<XmlDocument> GetConfig();
        string GetAppSettingsValue(XmlDocument xmlConfig, string key);
    }
}
