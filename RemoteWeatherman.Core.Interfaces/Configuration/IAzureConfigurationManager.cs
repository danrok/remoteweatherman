﻿using RemoteWeatherman.Core.Interfaces.Entities;
using System.Threading.Tasks;

namespace RemoteWeatherman.Core.Interfaces.Configuration
{
    public interface IAzureConfigurationManager
    {
        Task<AzureConfiguration> ReadAzureConfiguration();
    }
}
