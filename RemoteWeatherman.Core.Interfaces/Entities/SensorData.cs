﻿using System;

namespace RemoteWeatherman.Core.Interfaces.Entities
{
    public class SensorData
    {
        public SensorData(string deviceName)
        {
            this.Timestamp = DateTime.Now;
            this.deviceName = deviceName;
        }

        public string deviceName;
        public double Temperature { get; set; }
        public double Pressure { get; set; }
        public byte[] UncompestatedTemperature { get; set; }
        public byte[] UncompestatedPressure { get; set; }
        public DateTime Timestamp;
    }
}
