﻿namespace RemoteWeatherman.Core.Interfaces.Entities
{
    public class AzureConfiguration
    {
        public string DeviceId { get; set; }
        public string IotHubUri { get; set; }
        public string DeviceKey { get; set; }
    }
}
