﻿using RemoteWeatherman.Core.Interfaces.Entities;
using RemoteWeatherman.Core.Interfaces.Enums;
using System.Threading.Tasks;

namespace RemoteWeatherman.Core.Interfaces
{
    public interface IBmp180Sensor
    {
        Task InitializeAsync();
        Task<SensorData> GetSensorDataAsync(Bmp180AccuracyMode oss);
    }
}
